package de.ovgu.ccd.examples;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class CalculatorTest {
	
	Calculator calc;
	
	@Before
	public void setup(){
		calc = new Calculator();
	}

	@Test
	public void testAddZero() {
		assertEquals(0, calc.add(0, 0));
		assertEquals(1, calc.add(1, 0));
		assertEquals(Integer.MAX_VALUE, calc.add(Integer.MAX_VALUE, 0));	
	}
	
	@Test
	public void testAddOne() {
		assertEquals(1, calc.add(0, 1));
		assertEquals(2, calc.add(1, 1));
		assertEquals(Integer.MAX_VALUE, calc.add(Integer.MAX_VALUE - 1, 1));
	}

}
