package de.ovgu.ccd.examples;


/**
 * Utility class for performing some dummy calculations.
 */
public class Calculator {

	/**
	 * Calculates the sum of two integer variables.
	 */
	int add(int a, int b){
		return a+b;
	}

}
